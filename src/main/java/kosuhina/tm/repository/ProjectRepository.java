package kosuhina.tm.repository;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;

import java.util.*;

public class ProjectRepository {

    private List<Project>  projects = new ArrayList<>();
    private List<Project> projectsByName = new ArrayList<>();
    private Map<String, List<Project>> mapProject;

    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        if (mapProject == null) {
            mapProject = new HashMap<>();
        }
        if (mapProject.get(name) == null) {
            projectsByName = new ArrayList<>(); }
        projectsByName.add(project);
        mapProject.put(name, projectsByName);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        if (mapProject == null) {
            mapProject = new HashMap<>();
        }
        if (mapProject.get(name) == null) {
            projectsByName = new ArrayList<>(); }
        projectsByName.add(project);
        mapProject.put(name, projectsByName);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size() -1) return null;
        return projects.get(index);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public List<Project> findProjectsByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final List<Project> projectsByName = mapProject.get(name);
        if(projectsByName == null){
            return null;
        }
        return projectsByName;
    }

    public Project removeByIndex(final int index) {
        final Project task = findByIndex(index);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    public Project removeById(final Long id) {
        final Project task = findById(id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    public List<Project> removeByName(final String name) {
        final List<Project> projectsByName = findProjectsByName(name);
        if (projectsByName == null) return null;
        mapProject.remove(name);
        Iterator<Project> i = projects.iterator();
        while (i.hasNext()) {
            Project project = i.next();
            if (project.getName().equals(name)) i.remove();
        }
        return projectsByName;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: findAll()) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(project);
        }
        return result;
    }
}
