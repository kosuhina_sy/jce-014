package kosuhina.tm.controller;

import java.util.ArrayList;

public class CommandController<E> extends ArrayList<E>{
    private static CommandController history;
    private int maxSize;

    public static CommandController getHistory() {
        if (history == null) {
            history = new CommandController();
            history.maxSize = 10;
        }
        return history;
    }


    public CommandController(int size) {
        super();
        this.maxSize = size;
    }

    public CommandController() {
        super();
        this.maxSize = 10;
    }

    @Override
    public boolean add(E e) {
        if (this.size() > maxSize - 1) {
            this.remove(0);
        }
        return super.add(e);
    }

    public int getMaxSize() {
        return maxSize;
    }
}
