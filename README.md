# TASK-MANAGER

Учебный проект в рамках курса JAVA SPING.


# Оглавление


* [Требования к software](#software)

* [Требования к hardware](#hardware)

* [Описание стека технологий](#стек_технологий)
 
* [Разработчик](#разработчик)

* [Команда для сборки приложения](#сборка приложения)

* [Команда для запуска приложения](#команда для запуска приложения)

* [Поддерживаемые терминальные команды](#терминальные команды)




## Требования к software <a name = "software"> <a/> 

  Openjdk version "11"
  
  Apache Maven-3.6.3
  
## Требования к hardware <a name = "hardware"> <a/> 


  CPU i5 / i7
  
  RAM 8/16GB
  
  LAN / WI-FI
  
  SSD 40GB 
  

## Описание стека технологий <a name = "стек_технологий"> <a/>

  Java 
  
  Apache Maven

## Разработчик <a name = "разработчик"> <a/>

   Светлана Косухина
   
   e-mail <lana__svet@list.ru>  

## Команда для сборки приложения <a name = "сборка приложения"> <a/>

 ```
  mvn package
 ```

## Команда для запуска приложения <a name="команда для запуска приложения"> <a/>
```
 java -jar task-manager-1.0.0.jar
```

## Поддерживаемые терминальные команды <a name="терминальные команды"> <a/>


  help -отображение списка терминальных команд
  
  version - отображение информации о версии приложения
  
  about - отображение данных о разработчике
  
  command-history - отображение истории введенных команд
  
  exit - выход из приложения
  
  project-create - создание проекта
  
  project-clear - очистка списка проектов
  
  project-list - отображение списка проектов
      
  project-view - просмотр проекта
  
  project-remove-by-name - удаление проекта по имени
  
  project-remove-by-id - удаление проекта по коду
  
  project-remove-by-index - удаление проекта по индексу
      
  project-update-by-index - изменение проекта
  
  task-create - создание задачи
  
  task-clear - очистка списка задач
  
  task-list - отображение списка задач
  
  task-view - просмотр задачи
  
  task-remove-by-name - удаление задачи по имени
  
  task-remove-by-id - удаление задачи по коду
  
  task-remove-by-index - удаление задачи по индексу
  
  task-update-by-index - изменение задачи по индексу
  
  task-list-by-project-id - отображение списка задач по коду проекта
  
  task-add-to-project-by-ids - добавление задачи в проект по коду 
  
  task-remove-from-project-by-ids - удаление задачи из проекта по коду
  
  ask-add-to-user-by-index - добавление задачи текущему пользователю по индексу задачи
    
  task-list-profile - просмотр задач текущего пользователя
  
  user-create - создание нового пользователя.
  
  user-list - вывод на экран списка пользователей.
  
  user-clear - удаление всех пользователей
  
  user-update-by-id - изменение пользователя по id
  
  user-update-by-index -  изменение пользователя по индексу
  
  user-update-by-login - изменение пользователя по логину
  
  user-remove-by-id - удаление пользователя по id
  
  user-remove-by-index - удаление пользователя по индексу
  
  user-remove-by-login - удаление пользователя по логину
  
  user-view-by-id - просмотр пользователя по id
  
  user-view-by-index - просмотр пользователя по индексу
  
  user-view-by-login - просмотр пользователя по логину
  
  user-login - аутентификация пользователя
  
  user-logout - выход пользователя
  
  user-view-profile - проcмотр профиля текущего пользователя
  
  user-update-profile - изменение профиля текущего пользователя
  

  
  
  
  
  
  







